'use strict';

const filter = require('../index')

const assert = require('assert')

describe('filter', function() {
  it('returns attributes', function() {
    const meta = ['id', 'errors'],
          attributes = {id: 1, errors: {title: ['is blank']}}
    assert.deepEqual(filter(meta, attributes), attributes)
  })

  it('filters attributes', function() {
    const meta = ['id'],
          attributes = {id: 1, errors: {title: ['is blank']}}
    assert.deepEqual(filter(meta, attributes), {id: 1})
  })

  context('with nested', function() {
    it('returns attributes', function() {
      const meta = ['id', {
        diaries: [['title']]
      }],
            attributes = {
              id: 1,
              diaries: [
                {title: 'Morning tea'},
                {title: 'Xx'}
              ]
            }
      assert.deepEqual(filter(meta, attributes), attributes)
    })

    it('filters attributes', function() {
      const meta = ['id', {
        diaries: [['title']]
      }],
            attributes = {
              id: 1,
              diaries: [
                {visible: false},
                {title: 'Xx'}
              ]
            }
      assert.deepEqual(filter(meta, attributes), {
        id: 1,
        diaries: [
          {},
          {title: 'Xx'}
        ]
      })
    })
  })
})
