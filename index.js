'use strict';

function filter(meta, attributes) {
  if (Array.isArray(attributes)) {
    const array = attributes.map((attr) => filter(meta[0], attr))
    return array.filter((el) => el !== undefined)
  } else {
    const node = meta[meta.length - 1],
          object = {}
    Object.keys(attributes).forEach((attr) => {
      if (meta.includes(attr))
        object[attr] = attributes[attr]
      else if (node && typeof node !== 'string' && node[attr])
        object[attr] = filter(node[attr], attributes[attr])
    })
    return object
  }
}

module.exports = filter
